# README #

Simple script to create project directory with components.

## Usage 

From default script use ```packages.ini``` file as source of components but it can be overridden with option ```-l``` as on below example

```bash
PackageManager -l my_packages.ini
```
## Packages list file structure ##

Components are described by sections. Currently 3 types of components are supported: git, svn, binary file (single file or zip archive).
Example .ini file: ```packages.ini```.