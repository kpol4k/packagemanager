#!/usr/bin/python3

import argparse
import configparser
import os
import subprocess

def get_args():

    parser = argparse.ArgumentParser(description = "Prepare working directory in case of distributed subcomponents.")
    parser.add_argument('-l','--component-list',dest = 'component_list',help = 'File with list of commponents [default = packages.ini]',default = 'packages.ini')

    return parser.parse_args()

def check_config_section(parser):

    mandatory_fields = ['install_path']

    for field in mandatory_fields:
        if not parser.has_option('config',field):
            print('Default section lack mandatory option:',field)
            os.sys.exit(1)

def check_component_section(section,parser):

    section_types = ['git','svn','bin']
    mandatory_params = ['url','type']
    optional_params = ['branch','install_path','extract']

    #check mandatory options
    for option in mandatory_params:
        if not parser.has_option(section,option):
            print('Section',section + ':','Lack of mandatory option',option)
            return False

    if parser.get(section,'type') not in section_types:
        print('Section',section + ':','Unnsupported option type:',parser.get(section,'type'))
        return False

    for option in parser.options(section):
        if option not in mandatory_params and option not in optional_params:
            print('Section',section + ':','Unnsuported option:',option)
            return False
    return True

def get_component_from_repo(options):

    params = dict(options)

    if params['type'] == 'git':
        url = params['url']
        destination = params['install_path'] if 'install_path' in params else default_install_path
        branch =  params['branch'] if 'branch' in params else 'master'
        try:
            subprocess.check_call(['git','clone',url,'-b',branch,destination],stderr=subprocess.STDOUT)
        except:
            print('Unable to clone repository',url,'branch',branch)

    elif params['type'] == 'svn':
        pass

def get_binary_component(options):
    pass

def main():

    global default_install_path

    args = get_args()

    comp_file = args.component_list

    if not os.path.isfile(comp_file):
        print('Error: components list file', comp_file, ' not exist')
        os.sys.exit(1)
    components_list = configparser.SafeConfigParser()
    components_list.read(comp_file)

    check_config_section(components_list)
    # get data from config section
    default_install_path = components_list.get('config','install_path')

    components_list.remove_section('config')

    #parse rest sections

    for section in components_list.sections():
        if check_component_section(section,components_list):
            print('Get component',section,'type:',components_list.get(section,'type'))
            if components_list.get(section,'type') == 'bin':
                get_binary_component(components_list.items(section))
            elif components_list.get(section,'type') in ['git','svn']:
                get_component_from_repo(components_list.items(section))




if __name__ == '__main__':
    main()
    
    
